<?php

namespace TeamRock\ApplicationBundle\Controllers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Homepage
{
    public function __invoke(Request $request)
    {
        return new Response("Hello, world!", Response::HTTP_OK);
    }
}
