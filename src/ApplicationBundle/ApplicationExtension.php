<?php

namespace TeamRock\ApplicationBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

class ApplicationExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__.'/Configurations'));
        $loader->load('services.xml');
    }

    public function getConfiguration(array $config, ContainerBuilder $container)
    {
        return null;
    }
}
