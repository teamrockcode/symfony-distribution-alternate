FROM teamrock/apache2:development

RUN apt-get -y update && apt-get -y install libapache2-mod-php5
RUN ln -s /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-enabled/ && sed -i 's/html/web/' /etc/apache2/sites-available/000-default.conf

ENV SYMFONY__ENVIRONMENT="development"
ENV SYMFONY__DEBUG="true"

ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid

USER root

ADD run.sh /usr/local/bin/start.sh

ENTRYPOINT [ "/usr/local/bin/start.sh" ]
