<?php
use Symfony\Component\HttpFoundation\Request;

$loader = require_once __DIR__.'/../var/bootstrap.php.cache';

require_once __DIR__ . '/../src/AppKernel.php';

#$environment = array_key_exists('ENVIRONMENT', $_ENV) ? $_ENV['ENVIRONMENT'] : 'production';
#$debug = array_key_exists('DEBUG', $_ENV) ? boolval($_ENV['DEBUG']) : "false";

$environment = "development";
$debug = true;

$kernel = new AppKernel($environment, $debug);
$kernel->loadClassCache();

$request = Request::createFromGlobals();

$response = $kernel->handle($request);
$response->send();

$kernel->terminate($request, $response);
